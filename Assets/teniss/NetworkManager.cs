﻿using UnityEngine;
using System.Collections;

public class NetworkManager : MonoBehaviour {

	public GameObject cubePrefab;
	string ip = "127.0.0.1";
	string port = "1192";
	bool connected = false;
	int allowedNum = 32;
	
	private void CreatePlayer()
	{
		connected = true;
		Network.Instantiate(
			cubePrefab, 
			cubePrefab.transform.position,
			cubePrefab.transform.rotation,
			1);
	}

	public void OnConnectedToServer()
	{
		connected = true;
	}

	public void OnServerInitialized()
	{
		CreatePlayer();
	}


	public void OnGUI()
	{
		if (!connected) {
			if(GUI.Button (new Rect(10,10,90,90),"Client")){
				Network.Connect (ip, int.Parse (port));
			}
			if(GUI.Button (new Rect(10,110,90,90),"Master")){
				Network.InitializeServer (allowedNum, int.Parse (port), false);
			}		
		}
	}


	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
