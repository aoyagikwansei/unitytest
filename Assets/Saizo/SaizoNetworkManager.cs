﻿using UnityEngine;
using System.Collections;

public class SaizoNetworkManager : MonoBehaviour {

	private const string typeName = "TestSaizoApp";
	private const string gameName = "RoomA";

	public GameObject targetPrefabForMaster; // マスター用
	public GameObject targetPrefabForClient; //　クライアント用
	public GameObject targetPrefab;

	string ip = "127.0.0.1";
	string port = "1192";
	bool connected = false;
	bool master = false;
	int allowedNum = 32;

	// それぞれ自分のプレーヤーを生成
	private void CreatePlayer()
	{
		connected = true;
		Network.Instantiate(
			targetPrefab, 
			targetPrefab.transform.position,
			targetPrefab.transform.rotation,
			1);
	}

	// さーば(マスター)につなげたら
	public void OnConnectedToServer()
	{
		targetPrefab = targetPrefabForClient;

		connected = true;
		CreatePlayer();
	}

	// さーば(ますたー)を立ち上げたら
	public void OnServerInitialized()
	{
		targetPrefab = targetPrefabForMaster;
		CreatePlayer();
		master = true;
	}
	

	// 毎フレーム呼び出されUIを描く
	public void OnGUI()
	{
		if (!connected) {
				GUI.Label (new Rect (10, 10, 90, 40), "networkManager");

				if (GUI.Button (new Rect (10, 60, 90, 90), "Client")) {
						Network.Connect (ip, int.Parse (port));

				}
				if (GUI.Button (new Rect (10, 160, 90, 90), "Master")) {
						Network.InitializeServer (allowedNum, int.Parse (port), !Network.HavePublicAddress ());
						MasterServer.ipAddress = "127.0.0.1";
						MasterServer.RegisterHost (typeName, gameName);
						
				}		
		} else {
			if (master){
				GUI.Label (new Rect (10, 10, 200, 40), "You are master. push wsad.");
			}else{
				GUI.Label (new Rect (10, 10, 200, 40), "You are a client. push ↑↓←→ and space.");
			}
		}

	}

	private HostData[] hostList;
	
	private void RefreshHostList()
	{
		MasterServer.RequestHostList(typeName);
	}
	
	void OnMasterServerEvent(MasterServerEvent msEvent)
	{
		if (msEvent == MasterServerEvent.HostListReceived)
			hostList = MasterServer.PollHostList();
	}


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	

}
