﻿#pragma strict

var Accel = 1.0;

function Start () {

}

function Update () {
	// これで自分でつくったおぶじぇくとしか動かないらしい
	if (networkView.isMine)
	{
		Move();
	}

}

// WSADで動かす
function Move(){
	
	if (Input.GetKey(KeyCode.W)) {
		rigidbody.AddForce(
	    transform.forward * Accel,
	    ForceMode.Impulse
  		);
  	}
  	
	if (Input.GetKey(KeyCode.S)) {
		rigidbody.AddForce(
	    transform.forward * -1 * Accel,
	    ForceMode.Impulse
  		);
  	}
  	
	if (Input.GetKey(KeyCode.A)) {
		rigidbody.AddForce(
	    transform.right * -1 * Accel,
	    ForceMode.Impulse
  		);
  	}
  	
	if (Input.GetKey(KeyCode.D)) {
		rigidbody.AddForce(
	    transform.right  * Accel,
	    ForceMode.Impulse
  		);
  	}
	
}

function resetSize(){
	transform.localScale = new Vector3(1, 1, 1);
}