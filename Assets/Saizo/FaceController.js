﻿#pragma strict

var Accel = 1.0;

function Start () {

}

function Update () {

		// これで自分でつくったおぶじぇくとしか動かないらしい
		if (networkView.isMine)
        {
        	Move();
        }

}

// 動かす関数
function Move(){
	rigidbody.AddForce(
	    transform.right * Input.GetAxisRaw( "Horizontal" ) * Accel,
	    ForceMode.Impulse
  	);
  	
	rigidbody.AddForce(
	    transform.forward * Input.GetAxisRaw( "Vertical" ) * Accel,
	    ForceMode.Impulse
  	);
  	
  	// お遊び スペースで大きさ2倍
  	if (Input.GetKey(KeyCode.Space)) {
    	transform.localScale = new Vector3(2, 2, 2);
    	Invoke("resetSize", 1);
	} 
  	
}

function resetSize(){
	transform.localScale = new Vector3(1, 1, 1);
}