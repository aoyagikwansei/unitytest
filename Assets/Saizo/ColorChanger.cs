﻿using UnityEngine;
using System.Collections;

public class ColorChanger : MonoBehaviour {

	const string face = "Texture/face";
	const string blue = "Texture/tex_BtnDialOn";
	const string orange = "Texture/tex_BtnDialOff";
	Texture2D newTexture;
	Texture2D originalTexture;

	public GameObject saizoCube;


	// Use this for initialization
	void Start () {

		newTexture = Resources.Load(orange) as Texture2D; 
		originalTexture = Resources.Load(face) as Texture2D; 
	
	}
	
	// Update is called once per frame
	void Update () {
	
		if( networkView.isMine )
		{
			if( Input.GetMouseButtonDown(0) )
			{
				//ToggleColor();
				networkView.RPC("ToggleColor", RPCMode.All);
			}
		}
	}

	[RPC]
	void ToggleColor()
	{
		renderer.material.mainTexture = newTexture;
		Invoke ("resetColor", 1);
	}

	void resetColor(){
		renderer.material.mainTexture = originalTexture;
	}
}
